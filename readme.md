# ToC #

This repostitory hosts resources used in experiments conducted in "Maintenance Impact on Temporal Requirements: Model-Driven Development vs. Code-Based development" research:

### Content ###

* [Acme Application (IFML) Original version](https://drive.google.com/open?id=0Bzt2rY13F5eNX29zRDJfMWNERlk)
* [Acme Application (IFML) with Volatile Functionality](https://drive.google.com/open?id=0Bzt2rY13F5eNdHBPdlVSRl93M2c)
* [Petshop and booking applications SVN repositories](https://drive.google.com/open?id=0Bzt2rY13F5eNSEtsemtreU1EWEE)
* [Petshop and Booking Application's database for volatile version](https://drive.google.com/open?id=0Bzt2rY13F5eNd3h2UDh3c0JMcWc)
* [Petshop and Booking Application's database for original version](https://drive.google.com/open?id=0Bzt2rY13F5eNNVllRERndEFEWDQ)